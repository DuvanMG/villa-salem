from django.apps import AppConfig
import os
from django.conf import settings

class EMailConfig(AppConfig):
    name = 'e_mail'
    path = os.path.join(settings.BASE_DIR, 'e_mail')


