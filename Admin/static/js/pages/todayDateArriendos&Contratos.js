var date = new Date();

var day = date.getDate();
var month = date.getMonth() + 1;
var year = date.getFullYear();

if (month < 10) month = "0" + month;
if (day < 10) day = "0" + day;

var today = year + "/" + month + "/" + day;

// Asignación a los elementos con IDs:
document.getElementById("FechaArriendo").value = today;
document.getElementById("FechaContrato").value = today;


