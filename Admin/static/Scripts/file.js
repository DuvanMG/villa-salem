let doc;
window.onload=function(){
    doc=document;
    confugurarEventos();
    obtenerListaArchivos();
}

function confugurarEventos(){

    let fuArchivo=doc.getElementById("fuArchivo");
    fuArchivo.onchange=function(){
        let f=this.files[0];
        let n=f.name;
        let lbln=doc.getElementById("txtNombreARchivo");
        lbln.innerHTML=n;
    }

    let btnEnviar=doc.getElementById("btnEnviar");
    btnEnviar.onclick=function(){

        let fuArchivo=doc.getElementById("fuArchivo");
        let f=fuArchivo.files[0];
        if(f){
            enviarArchivo(f,f.name,0,1024*1024*2)

        }else{
            alert("Seleccione archivo a enviar!!!");
        }
    }

    let ulArchivos=doc.getElementById("ulArchivos");
    ulArchivos.onclick=function(e){
        let li=e.target||e.srcElement;
        if(li.nodeName=="LI"){
            let n=li.textContent;
            let ifmVista=doc.getElementById("ifmVista");
            ifmVista.src="media/"+n;

        }
    }

    let btnLimpiar=doc.getElementById("btnLimpiar");
    btnLimpiar.onclick=function(){
        let fuArchivo=doc.getElementById("fuArchivo");
        fuArchivo.value="";
        let lbln=doc.getElementById("txtNombreARchivo");
        lbln.innerHTML="Seleccione un archivo!!";
        let divProgress=doc.getElementById("divProgress");
        divProgress.style.cssText="width:0px;"
    }
}

function obtenerListaArchivos(){

    http("get","listarArchivos",null,null,null).then(function(d){

        if(d){
            let l=d.split("|");
            let n=l.length,html=[];
            for(let i=0;i<n;i++){
                
                html.push("<li class='ul_archivo_li'>");
                html.push(l[i]);
                html.push("</li>");

            }
            let ulArchivos=doc.getElementById("ulArchivos");
            ulArchivos.innerHTML=html.join("");

        }

    });

}

function enviarArchivo(f,n,i,l){

    let size=f.size;
    let part=f.slice(i,(i+l))
    let divProgress=doc.getElementById("divProgress");

    http("post","subirArchivo",part,null,n).then(function(d){

        if(d=="Ok"){
            if(i<=size){
                let p=((i+l)*100)/size|0;
                p=(p>100?100:p);
                divProgress.style.cssText="width:"+(p)+"%;";

                enviarArchivo(f,n,(i+l),l);

            }else{
                obtenerListaArchivos();
                divProgress.style.cssText="width:100%;";
                alert("Archivo subito");
            }
        }else{
            alert("Error en envio de archivo");
        }
    });
}

function http(tipo,url,data,resp,na){

    return new Promise(function(resolve,reject){

        let csrftoken=document.getElementsByName("csrfmiddlewaretoken")[0].value;
        let xhr=new XMLHttpRequest();
        xhr.open(tipo,url);
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
        if(na!=null){
            xhr.setRequestHeader("archivo", na);
        }
        resp && (xhr.responseType = 'arraybuffer');

        xhr.onreadystatechange=function(){
            if(xhr.readyState==4 && xhr.status==200){
                resolve(resp?xhr.response:xhr.responseText);
            }
        }

        xhr.onerror=function(){
            resolve("Error");
        }

        xhr.send(data?data:null);
    });
}