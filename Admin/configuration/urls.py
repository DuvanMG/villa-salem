"""skote URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from configuration import views
from .views import MyPasswordSetView ,MyPasswordChangeView
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),

    # Dashboards View
    path('',views.DashboardView.as_view(),name='dashboard'),
    # Layouts
    path('layout/',include('layout.urls')),
    # File manager View
    path('filemanager/',include('filemanager.urls')),
    #Email
    path("email/",include("e_mail.urls")),
    #Pages
    path('pages/',include('pages.urls')),
    #general
    path('general/',include('general.urls')),
    #prestamos
    path('prestamos/',include('prestamos.urls')),
    #Arriendos
    path('arriendos/',include('arriendos.urls')),
    #Creditos
    path('creditos/',include('creditos.urls')),
    #Finca
    path('finca/',include('finca.urls')),
    #Clientes
    path('clientes/',include('clientes.urls')),
    #Inventario
    path('inventario/',include('inventario.urls')),
    # Allauth
    path('account/', include('allauth.urls')),
    path('auth-logout/',TemplateView.as_view(template_name="account/logout-success.html"),name ='pages-logout'),
    path('auth-lockscreen/',TemplateView.as_view(template_name="account/lock-screen.html"),name ='pages-lockscreen'),
        #Custum change password done page redirect
    path('accounts/password/change/', login_required(MyPasswordChangeView.as_view()), name="account_change_password"),
    #Custum set password done page redirect
    path('accounts/password/set/', login_required(MyPasswordSetView.as_view()), name="account_set_password"),
]
# urlpatterns = [
#  # .......
# ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns+=static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)