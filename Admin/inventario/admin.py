from django.contrib import admin
from .models import Mantenimientos, Cocina

# Register your models here.
admin.site.register(Mantenimientos)
admin.site.register(Cocina)

