from multiprocessing import context
from urllib import request
from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Mantenimientos, Cocina
from .forms import MantenimientoForm, CocinaForm

class MantenimientoView(LoginRequiredMixin,View):
    def get(self , request):

        heading = "Inventario"
        pageview = "Villa Salem"

        #convirtiendo el objeto en un diccionario
        Mantenimiento =  Mantenimientos.objects.all()
        cocina = Cocina.objects.all()

        context = {
            'Mantenimientos':Mantenimiento,
            'Cocina':cocina,
            'heading':heading,
            'pageview':pageview,
            'form':MantenimientoForm()
        }
  
        return render(request,'components/tables/components-inventario.html',context)
         
    def post(self,request):
        #convirtiendo el objeto en un diccionario
        formulario = MantenimientoForm(request.POST)
        if formulario.is_valid:
            formulario.save()
            return redirect('/inventario')

def updateMantenimiento(request,Id_Mantenimiento):

    if request.method == 'POST':
        #convirtiendo el objeto en un diccionario
        form = Mantenimientos.objects.get(Id_Mantenimiento = Id_Mantenimiento)

        formulario = MantenimientoForm(request.POST, instance = form)
        if formulario.is_valid:
            formulario.save()
            return redirect('/inventario')

def deleteMantenimiento(request,Id_Mantenimiento):

    #convirtiendo el objeto en un diccionario
    Mantenimientos.objects.filter(Id_Mantenimiento = Id_Mantenimiento).delete()

    return redirect('/inventario')

def CrearProductoCocina(request):

    if request.method == 'POST':
        #convirtiendo el objeto en un diccionario
        formulario = CocinaForm(request.POST)
        if formulario.is_valid:
            formulario.save()
            return redirect('/inventario')

def updateCocina(request,Id_Cocina):

    if request.method == 'POST':
        #convirtiendo el objeto en un diccionario
        form = Cocina.objects.get(Id_Cocina = Id_Cocina)

        formulario = CocinaForm(request.POST, instance = form)
        if formulario.is_valid:
            formulario.save()
            return redirect('/inventario')

def deleteCocina(request,Id_Cocina):

    #convirtiendo el objeto en un diccionario
    Cocina.objects.filter(Id_Cocina = Id_Cocina).delete()

    return redirect('/inventario')