from django.apps import AppConfig
import os
from django.conf import settings

class InventarioConfig(AppConfig):
    name = 'inventario'
    path = os.path.join(settings.BASE_DIR, 'inventario')
