from django.urls import path
from inventario import views

urlpatterns = [
    #Mantenimiento
    path('', views.MantenimientoView.as_view(), name='table-inventario'),
    path('updateMantenimiento/<str:Id_Mantenimiento>',views.updateMantenimiento,name='updateMantenimiento'),
    path('deleteMantenimiento/<str:Id_Mantenimiento>',views.deleteMantenimiento,name='deleteMantenimiento'),

    #Cocina
    path('CrearProductoCocina/', views.CrearProductoCocina, name='CrearProductoCocina'),
    path('updateCocina/<str:Id_Cocina>',views.updateCocina,name='updateCocina'), 
    path('deleteCocina/<str:Id_Cocina>',views.deleteCocina,name='deleteCocina')
]