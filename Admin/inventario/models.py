from django.db import models

# Create your models here.
class Mantenimientos(models.Model):
    Id_Mantenimiento = models.AutoField(primary_key=True, serialize=False)
    FechaMantenimiento = models.CharField(max_length=70, default='', null=False)
    NombreMantenimiento = models.CharField(max_length=70, default='', null=False)
    CantidadProductoMantenimiento = models.IntegerField()

    class Meta:
        ordering = ('-FechaMantenimiento','-Id_Mantenimiento')

class Cocina(models.Model):
    Id_Cocina = models.AutoField(primary_key=True, serialize=False)
    FechaCocina = models.CharField(max_length=70, default='', null=False)
    NombreCocina = models.CharField(max_length=70, default='', null=False)
    CantidadProductoCocina = models.IntegerField()

    class Meta:
        ordering = ('-FechaCocina','-Id_Cocina')