from dataclasses import fields
from django import forms
from django.forms import ModelForm
from .models import Mantenimientos, Cocina

class MantenimientoForm(ModelForm):
    class Meta:
        model = Mantenimientos
        fields = ['FechaMantenimiento','NombreMantenimiento','CantidadProductoMantenimiento']

class CocinaForm(ModelForm):
    class Meta:
        model = Cocina
        fields = ['FechaCocina','NombreCocina','CantidadProductoCocina']