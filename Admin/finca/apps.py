from django.apps import AppConfig
import os
from django.conf import settings

class FincaConfig(AppConfig):
    name = 'finca'
    path = os.path.join(settings.BASE_DIR, 'finca')

