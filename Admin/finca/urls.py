from django.urls import path
from finca import views

urlpatterns = [
    #Eventos URLs
    path('eventos',views.CalendarEventosView.as_view(),name='calendar-eventos'),
    path('tablaEventos',views.tablaEventos,name='tabla-eventos'),
    path('updateEvento/<str:Id_Evento>',views.updateEvento,name='updateEvento'),
    path('deleteEvento/<str:Id_Evento>',views.deleteEvento,name='deleteEvento'),
    #Tareas URLs
    path('tareas', views.CalendarTareasView.as_view(),name='calendar-tareas'),
    path('tablaTareas',views.tablaTareas,name='tabla-tareas'),
    path('updateTarea/<str:Id_Tarea>',views.updateTarea,name='updateTarea'),
    path('deleteTarea/<str:Id_Tarea>',views.deleteTarea,name='deleteTarea'),
    #Filemanager URLs
    # path('filemanager',views.filemanager,name='filemanager'),
    # path('subirArchivo', views.subirArchivo,name='subirArchivo'),
    # path('listarArchivos', views.listarArchivos,name='listarArchivos'),

]    