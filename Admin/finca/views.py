from multiprocessing import context
from urllib import request
from django.shortcuts import render, redirect
from django.views import View
from django.views.generic import ListView 
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Finca, Eventos, Tareas
from .forms import EventoForm, TareaForm
from django.http import HttpResponse
from django.conf import settings
import os
from django.views.decorators.clickjacking import xframe_options_sameorigin
from django.views.decorators.gzip import gzip_page
from compression_middleware.decorators import compress_page

class CalendarEventosView(LoginRequiredMixin,ListView):
    def get(self , request):

        heading = "Calendario de Eventos"
        pageview = "Finca"

        #convirtiendo el objeto en un diccionario
        Evento =  Eventos.objects.all()
        event_list = []
        # Agrega los datos al diccionario
        for event in Evento:
            event_list.append(
                {
                    "title": event.pastor,
                    "start": event.InicioEvento.strftime("%Y-%m-%d"),
                    "end": event.FinalEvento.strftime("%Y-%m-%d"),
                    "color": event.Color

                }
            )
        context = {
            "events": event_list,
            'heading':heading,
            'pageview':pageview,
            'form':EventoForm()
        }
      
        return render(request,'components/calendars/calendarEventos.html', context)

    def post(self,request):

        heading = "Calendario de Eventos"
        pageview = "Finca"

        #convirtiendo el objeto en un diccionario
        Evento = Eventos.objects.all()
        event_list = []

        formulario = EventoForm(request.POST)
        if formulario.is_valid:
            formulario.save()       

        # Agrega los datos al diccionario
        for event in Evento:
            event_list.append(
                {
                    "title": event.pastor,
                    "start": event.InicioEvento.strftime("%Y-%m-%d"),
                    "end": event.FinalEvento.strftime("%Y-%m-%d"),
                    "color": event.Color

                }
            )
        context = {
            "events": event_list,
            'heading':heading,
            'pageview':pageview,
            'form':EventoForm()
        }

        return render(request,'components/calendars/calendarEventos.html',context)

def tablaEventos(request):

    heading = "Tabla de Eventos"
    pageview = "Finca"
    #convirtiendo el objeto en un diccionario
    Evento = Eventos.objects.all()

    context = {
        'Eventos':Evento,
        'heading':heading,
        'pageview':pageview,
        'form':EventoForm()
    }

    return render(request,'components/tables/components-eventos.html',context)

def updateEvento(request,Id_Evento):

    if request.method == 'POST':
        #convirtiendo el objeto en un diccionario
        form = Eventos.objects.get(Id_Evento = Id_Evento)

        formulario = EventoForm(request.POST, instance = form)
        if formulario.is_valid:
            formulario.save()
            return redirect('/finca/tablaEventos')

def deleteEvento(request,Id_Evento):

    #convirtiendo el objeto en un diccionario
    Eventos.objects.filter(Id_Evento = Id_Evento).delete()

    return redirect('/finca/tablaEventos')

class CalendarTareasView(LoginRequiredMixin,ListView):
    def get(self , request):

        heading = "Calendario de Tareas"
        pageview = "Finca"

        #convirtiendo el objeto en un diccionario
        Tarea =  Tareas.objects.all()
        event_list = []
        # Agrega los datos al diccionario
        for event in Tarea:
            event_list.append(
                {
                    "title": event.Ntarea,
                    "start": event.InicioActividad.strftime("%Y-%m-%d"),
                    # "start": event.InicioActividad.strftime("%Y-%m-%dT%H:%M:%S"),
                    "end": event.FinalActividad.strftime("%Y-%m-%d"),
                    "color": event.Estado,
                    "description": event.Actividades

                }
            )
        context = {
            "events": event_list,
            'heading':heading,
            'pageview':pageview,
            'form':TareaForm()
        }
        
        return render(request,'components/calendars/calendarTareas.html', context)
         
    def post(self,request):

        heading = "Calendario de Tareas"
        pageview = "Finca"

        Tarea =  Tareas.objects.all()
        event_list = []

        #convirtiendo el objeto en un diccionario
        formulario = TareaForm(request.POST)
        if formulario.is_valid:
            formulario.save()

        # Agrega los datos al diccionario
        for event in Tarea:
            event_list.append(
                {
                    "title": event.Ntarea,
                    "start": event.InicioActividad.strftime("%Y-%m-%dT%H:%M:%S"),
                    "end": event.FinalActividad.strftime("%Y-%m-%dT%H:%M:%S"),
                    "color": event.Estado,
                    "description": event.Actividades

                }
            )
        context = {
            "events": event_list,
            'heading':heading,
            'pageview':pageview,
            'form':TareaForm()
        }
     
        return render(request,'components/calendars/calendarTareas.html',context)

def tablaTareas(request):

    heading = "Tabla de Tareas"
    pageview = "Finca"
    #convirtiendo el objeto en un diccionario
    Tarea = Tareas.objects.all()

    context = {
        'Tareas':Tarea,
        'heading':heading,
        'pageview':pageview,
        'form':TareaForm()
    }

    return render(request,'components/tables/components-tareas.html',context)

def updateTarea(request,Id_Tarea):

    if request.method == 'POST':
        #convirtiendo el objeto en un diccionario
        form = Tareas.objects.get(Id_Tarea = Id_Tarea)

        formulario = TareaForm(request.POST, instance = form)
        if formulario.is_valid:
            formulario.save()
            return redirect('/finca/tablaTareas')

def deleteTarea(request,Id_Tarea):

    #convirtiendo el objeto en un diccionario
    Tareas.objects.filter(Id_Tarea = Id_Tarea).delete()

    return redirect('/finca/tablaTareas')

# @compress_page
# def filemanager(request):
#     contexto={"v":"1"}
#     return render(request, 'components/file/filemanager.html', contexto)

# def subirArchivo(request):
#     rpta=""
#     if "archivo" in request.headers:
#         nombre=request.headers["archivo"]
#         archivo=os.path.join(settings.BASE_DIR,'media',nombre)

#         with open(archivo,'ab') as f:
#             f.write(request.body)
        
#         rpta="Ok"
        
#     return HttpResponse(str(rpta))

# @gzip_page
# def listarArchivos(request):

#     rpta=[]
#     ruta=os.path.join(settings.BASE_DIR,'media')
#     print(ruta)
#     for root,dir,files in os.walk(ruta):
#         for j in files:
#             rpta.append(j)
          
#     return HttpResponse("|".join(rpta))