from dataclasses import fields
from django import forms
from django.forms import ModelForm
from .models import Eventos, Tareas

class EventoForm(ModelForm):
    class Meta:
        model = Eventos
        fields = ['Color','pastor','Celular','Abono','ValorPersona','InicioEvento','FinalEvento','Actividades','Comida']

class TareaForm(ModelForm):
    class Meta:
        model = Tareas
        fields = ['Estado','Ntarea','InicioActividad','FinalActividad','Actividades']