from django.db import models

# Create your models here.
class Finca(models.Model):
    idFinca = models.CharField(max_length=30, default='', null=False)
    nombre = models.CharField(max_length=30, default='', null=False)

    class Meta:
        ordering = ('-idFinca',)

class Eventos(models.Model):
    Id_Evento = models.AutoField(primary_key=True, serialize=False)
    Color = models.CharField(max_length=70, default='', null=False)
    pastor = models.CharField(max_length=70, default='', null=False)
    Celular = models.CharField(max_length=70, default='', null=False)
    Abono = models.CharField(max_length=70, default='', null=False)
    ValorPersona = models.CharField(max_length=70, default='', null=False)
    InicioEvento = models.CharField(max_length=70, default='', null=False)
    FinalEvento = models.CharField(max_length=70, default='', null=False)
    Actividades = models.CharField(max_length=300, default='', null=False)
    Comida = models.CharField(max_length=70, default='', null=False)

    class Meta:
        ordering = ('-InicioEvento',)

class Tareas(models.Model):
    Id_Tarea = models.AutoField(primary_key=True, serialize=False)
    Estado = models.CharField(max_length=70, default='', null=False)
    Ntarea = models.CharField(max_length=70, default='', null=False)
    InicioActividad = models.CharField(max_length=70, default='', null=False)
    FinalActividad = models.CharField(max_length=70, default='', null=False)
    Actividades = models.CharField(max_length=300, default='', null=False)

    class Meta:
        ordering = ('-InicioActividad',)