from django.apps import AppConfig
import os
from django.conf import settings

class ArriendosConfig(AppConfig):
    name = 'arriendos'
    path = os.path.join(settings.BASE_DIR, 'arriendos')