# Generated by Django 4.1.1 on 2022-10-24 10:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Arriendos',
            fields=[
                ('Id_Arriendo', models.AutoField(primary_key=True, serialize=False)),
                ('FechaArriendo', models.CharField(default='', max_length=70)),
                ('NombreArriendo', models.CharField(default='', max_length=70)),
                ('PropiedadArriendo', models.CharField(default='', max_length=70)),
                ('ValorArriendo', models.CharField(default='', max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Contratos',
            fields=[
                ('Id_Contrato', models.AutoField(primary_key=True, serialize=False)),
                ('FechaContrato', models.CharField(default='', max_length=70)),
                ('NombreContrato', models.CharField(default='', max_length=70)),
                ('PropiedadContrato', models.CharField(choices=[('Apartamento 108', 'Apartamento 108'), ('Piso 1 de la 43', 'Piso 1 de la 43'), ('Piso 2 de la 43', 'Piso 2 de la 43'), ('Piso 3 de la 43', 'Piso 3 de la 43'), ('Piso 4 de la 43', 'Piso 4 de la 43'), ('Local 9', 'Local 9'), ('Apto 101 Finca', 'Apto 101 Finca'), ('Apto 201 Finca', 'Apto 201 Finca'), ('Lote Mesa de los Santos', 'Lote Mesa de los Santos')], default='', max_length=70)),
                ('ValorContrato', models.CharField(default='', max_length=50)),
                ('TipoContrato', models.CharField(default='', max_length=70)),
            ],
        ),
        migrations.CreateModel(
            name='Fiador',
            fields=[
                ('Id_Fiador', models.AutoField(primary_key=True, serialize=False)),
                ('CelularContrato', models.CharField(default='', max_length=70)),
                ('DireccionContratoCasa', models.CharField(default='', max_length=70)),
                ('DireccionContratoTrabajo', models.CharField(default='', max_length=70)),
                ('NombreFiador', models.CharField(default='', max_length=70)),
                ('CelularFiador', models.CharField(default='', max_length=70)),
                ('DireccionFiadorCasa', models.CharField(default='', max_length=70)),
                ('DireccionFiadorTrabajo', models.CharField(default='', max_length=70)),
                ('Contrato_Id', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='arriendos.contratos')),
            ],
        ),
    ]
