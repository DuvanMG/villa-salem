from django.urls import path
from arriendos import views

urlpatterns = [
    #Prestamos
    path('',views.ContratosView.as_view(),name='tables-arriendos'),
    path('CrearFiador/',views.CrearFiador,name='CrearFiador'),
    path('updateFiador/<str:Id_Fiador>',views.updateFiador,name='updateFiador'),
    path('updateContrato/<str:Id_Contrato>',views.updateContrato,name='updateContrato'),
    path('deleteContrato/<str:Id_Contrato>',views.deleteContrato,name='deleteContrato'),

    #Cobros
    path('CrearArriendo/',views.ArriendosView,name='CrearArriendo'),
    path('updateArriendo/<str:Id_Arriendo>',views.updateArriendo,name='updateArriendo'),
    path('deleteArriendo/<str:Id_Arriendo>',views.deleteArriendo,name='deleteArriendo')
]    