from dataclasses import fields
from django import forms
from django.forms import ModelForm
from .models import Contratos, Arriendos, Fiador

class ContratosForm(ModelForm):
    class Meta:
        model = Contratos
        fields = ['FechaContrato','NombreContrato','PropiedadContrato','ValorContrato','TipoContrato']

class FiadorForm(ModelForm):
    class Meta:
        model = Fiador
        fields = ['Contrato_Id','CelularContrato','DireccionContratoCasa','DireccionContratoTrabajo','NombreFiador','CelularFiador','DireccionFiadorCasa','DireccionFiadorTrabajo']

class ArriendosForm(ModelForm):
    class Meta:
        model = Arriendos
        fields = ['FechaArriendo','NombreArriendo','PropiedadArriendo','ValorArriendo']