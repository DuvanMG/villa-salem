from multiprocessing import context
from urllib import request
from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Contratos, Fiador, Arriendos
from .forms import ContratosForm, FiadorForm, ArriendosForm

# Create your views here - Contratos.
class ContratosView(LoginRequiredMixin,View):

    def get(self,request):

        heading = "Contratos"
        pageview = "Contratos & Arriendos"

        #convirtiendo el objeto en un diccionario
        contratos = Contratos.objects.all()
        fiador = Fiador.objects.select_related('Contrato_Id')
        arriendos = Arriendos.objects.all()

        context = {
            'Contratos':contratos,
            'Fiador':fiador,
            'Arriendos':arriendos,
            'heading':heading,
            'pageview':pageview,
            'form':ContratosForm()
        }

        return render (request,'components/tables/components-arriendos.html',context) 
         
    def post(self,request):
        #convirtiendo el objeto en un diccionario
        formulario = ContratosForm(request.POST)
        if formulario.is_valid:
            formulario.save()
            return redirect('/arriendos')

def CrearFiador(request):

    if request.method == 'POST':
        #convirtiendo el objeto en un diccionario
        formulario = FiadorForm(request.POST)
        if formulario.is_valid:
            formulario.save()
            return redirect('/arriendos')
    
def updateFiador(request,Id_Fiador):

    if request.method == 'POST':
        #convirtiendo el objeto en un diccionario
        form = Fiador.objects.get(Id_Fiador = Id_Fiador)
        formulario = FiadorForm(request.POST, instance = form)
        
        if formulario.is_valid:
            formulario.save()
            return redirect('/arriendos')
    
def updateContrato(request,Id_Contrato):

    if request.method == 'POST':
        #convirtiendo el objeto en un diccionario
        form = Contratos.objects.get(Id_Contrato = Id_Contrato)
        formulario = ContratosForm(request.POST, instance = form)
        
        if formulario.is_valid:
            formulario.save()
            return redirect('/arriendos')

def deleteContrato(request,Id_Contrato):

    #convirtiendo el objeto en un diccionario
    Contratos.objects.filter(Id_Contrato = Id_Contrato).delete()

    return redirect('/arriendos')

# Create your views here - Arriendos.         
def ArriendosView(request):

    if request.method == 'POST':
        #convirtiendo el objeto en un diccionario
        formulario = ArriendosForm(request.POST)
        if formulario.is_valid:
            formulario.save()
            return redirect('/arriendos')
    
def updateArriendo(request,Id_Arriendo):

    if request.method == 'POST':
        #convirtiendo el objeto en un diccionario
        form = Arriendos.objects.get(Id_Arriendo = Id_Arriendo)

        formulario = ArriendosForm(request.POST, instance = form)
        if formulario.is_valid:
            formulario.save()
            return redirect('/arriendos')

def deleteArriendo(request,Id_Arriendo):

    #convirtiendo el objeto en un diccionario
    Arriendos.objects.filter(Id_Arriendo = Id_Arriendo).delete()

    return redirect('/arriendos')
