from django.db import models
from .choices import propiedad
# Create your models here.

class Arriendos(models.Model):
    Id_Arriendo = models.AutoField(primary_key=True, serialize=False)
    FechaArriendo = models.CharField(max_length=70, default='', null=False)
    NombreArriendo = models.CharField(max_length=70, default='', null=False)
    PropiedadArriendo = models.CharField(max_length=70, default='', null=False)
    ValorArriendo = models.CharField(max_length=50, default='', null=False)

    class Meta:
        ordering = ('-FechaArriendo','-Id_Arriendo')
        
class Contratos(models.Model):
    Id_Contrato = models.AutoField(primary_key=True, serialize=False)
    FechaContrato = models.CharField(max_length=70, default='', null=False)
    NombreContrato = models.CharField(max_length=70, default='', null=False)
    PropiedadContrato = models.CharField(max_length=70, choices=propiedad, default='')
    ValorContrato = models.CharField(max_length=50, default='', null=False)
    TipoContrato = models.CharField(max_length=70, default='', null=False)

    class Meta:
        ordering = ('-FechaContrato','-Id_Contrato')

class Fiador(models.Model):
    Id_Fiador = models.AutoField(primary_key=True, serialize=False)
    Contrato_Id = models.OneToOneField(Contratos, on_delete = models.CASCADE, null=False)
    CelularContrato = models.CharField(max_length=70, default='', null=False)
    DireccionContratoCasa = models.CharField(max_length=70, default='', null=False)
    DireccionContratoTrabajo = models.CharField(max_length=70, default='', null=False)
    NombreFiador = models.CharField(max_length=70, default='', null=False)
    CelularFiador = models.CharField(max_length=70, default='', null=False)
    DireccionFiadorCasa = models.CharField(max_length=70, default='', null=False)
    DireccionFiadorTrabajo = models.CharField(max_length=70, default='', null=False)

    def __str__(self):
        return self.NombreFiador

