from django.urls import path
from creditos import views

urlpatterns = [
    #Creditos
    path('',views.CreditosView.as_view(),name='tables-creditos'),
    path('updateCredito/<str:Id_Credito>',views.updateCredito,name='updateCredito'),
    path('deleteCredito/<str:Id_Credito>',views.deleteCredito,name='deleteCredito'),

    #Cuotas
    path('CrearCuota/',views.CuotasView,name='CrearCuota'),
    path('updateCuota/<str:Id_Cuota>',views.updateCuota,name='updateCuota'),
    path('deleteCuota/<str:Id_Cuota>',views.deleteCuota,name='deleteCuota')
]