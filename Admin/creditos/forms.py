from dataclasses import fields
from django import forms
from django.forms import ModelForm
from .models import Creditos, Cuotas

class CreditoForm(ModelForm):
    class Meta:
        model = Creditos
        fields = ['FechaCredito','NombreCredito','ValorCredito','PorcentajeCredito','FechaPagoCredito','ValorCuota','CuotasCredito','DescripcionCredito']

class CuotaForm(ModelForm):
    class Meta:
        model = Cuotas
        fields = ['FechaCuota','ValorCuota','NombreCuota']