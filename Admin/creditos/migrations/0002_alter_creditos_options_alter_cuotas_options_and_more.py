# Generated by Django 4.1.1 on 2022-11-17 23:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('creditos', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='creditos',
            options={'ordering': ('-FechaCredito', '-Id_Credito')},
        ),
        migrations.AlterModelOptions(
            name='cuotas',
            options={'ordering': ('-FechaCuota', '-Id_Cuota')},
        ),
    ]
