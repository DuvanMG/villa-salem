from django.db import models

# Create your models here.
class Creditos(models.Model):
    Id_Credito = models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")
    FechaCredito = models.CharField(max_length=70, default='', null=False)
    NombreCredito = models.CharField(max_length=70, default='', null=False)
    ValorCredito = models.CharField(max_length=50, default='', null=False)
    PorcentajeCredito = models.CharField(max_length=70, default='', null=False)
    FechaPagoCredito = models.CharField(max_length=70, default='', null=False)
    ValorCuota = models.CharField(max_length=50, default='', null=False)
    CuotasCredito = models.CharField(max_length=50, default='', null=False)
    DescripcionCredito = models.CharField(max_length=300, default='', null=False)

    class Meta:
        ordering = ('-FechaCredito','-Id_Credito')

# Create your models here.
class Cuotas(models.Model):
    Id_Cuota = models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")
    FechaCuota = models.CharField(max_length=70, default='', null=False)
    ValorCuota = models.CharField(max_length=50, default='', null=False)
    NombreCuota = models.CharField(max_length=70, default='', null=False)

    class Meta:
        ordering = ('-FechaCuota','-Id_Cuota')