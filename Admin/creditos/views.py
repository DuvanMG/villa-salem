from multiprocessing import context
from urllib import request
from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Creditos, Cuotas
from .forms import CreditoForm, CuotaForm

# Create your views here - Creditos
class CreditosView(LoginRequiredMixin,View):

    def get(self , request):

        heading = "Creditos"
        pageview = "Creditos & Cuotas"

        #convirtiendo el objeto en un diccionario
        creditos =  Creditos.objects.all()
        cuotas =  Cuotas.objects.all()

        context = {
            'Creditos':creditos,
            'Cuotas':cuotas,
            'heading':heading,
            'pageview':pageview,
            'form':CreditoForm()
        }
  
        return render(request,'components/tables/components-creditos.html',context)
         
    def post(self,request):

        formulario = CreditoForm(request.POST)
        if formulario.is_valid:
            formulario.save()
            return redirect('/creditos')

def updateCredito(request,Id_Credito):

    if request.method == 'POST':
        #convirtiendo el objeto en un diccionario
        form = Creditos.objects.get(Id_Credito = Id_Credito)

        formulario = CreditoForm(request.POST, instance = form)
        if formulario.is_valid:
            formulario.save()
            return redirect('/creditos')

def deleteCredito(request,Id_Credito):

    #convirtiendo el objeto en un diccionario
    Creditos.objects.filter(Id_Credito = Id_Credito).delete()

    return redirect('/creditos')

# Create your views here - Cuotas         
def CuotasView(request):

    if request.method == 'POST':
    #convirtiendo el objeto en un diccionario
        formulario = CuotaForm(request.POST)
        if formulario.is_valid:
            formulario.save()
            return redirect('/creditos')

def updateCuota(request,Id_Cuota):

    if request.method == 'POST':
        #convirtiendo el objeto en un diccionario
        form = Cuotas.objects.get(Id_Cuota = Id_Cuota)

        formulario = CuotaForm(request.POST, instance = form)
        if formulario.is_valid:
            formulario.save()
            return redirect('/creditos')

def deleteCuota(request,Id_Cuota):

    #convirtiendo el objeto en un diccionario
    Cuotas.objects.filter(Id_Cuota = Id_Cuota).delete()

    return redirect('/creditos')