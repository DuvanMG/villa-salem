from django.apps import AppConfig
import os
from django.conf import settings

class CreditosConfig(AppConfig):
    name = 'creditos'
    path = os.path.join(settings.BASE_DIR, 'creditos')