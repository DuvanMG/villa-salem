from django.apps import AppConfig
import os
from django.conf import settings

class FileConfig(AppConfig):
    name = 'filemanager'
    path = os.path.join(settings.BASE_DIR, 'filemanager')

