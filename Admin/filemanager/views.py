from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
import os
from django.views.decorators.clickjacking import xframe_options_sameorigin
from django.views.decorators.gzip import gzip_page
from compression_middleware.decorators import compress_page

@compress_page
def filemanager(request):
    heading = "Administrador de archivos"
    pageview = "Villa Salem"  

    contexto = {
        "v":"1",
        'pageview':pageview,
        'heading':heading
    }
    return render(request, 'components/file/filemanager.html', contexto)

def subirArchivo(request):
    rpta=""
    if "archivo" in request.headers:
        nombre=request.headers["archivo"]
        archivo=os.path.join(settings.BASE_DIR,'filemanager/media',nombre)

        with open(archivo,'ab') as f:
            f.write(request.body)
        
        rpta="Ok"
        
    return HttpResponse(str(rpta))

@gzip_page
def listarArchivos(request):

    rpta=[]
    ruta=os.path.join(settings.BASE_DIR,'filemanager/media')
    
    for root,dir,files in os.walk(ruta):
        for j in files:
            rpta.append(j)
          
    return HttpResponse("|".join(rpta))