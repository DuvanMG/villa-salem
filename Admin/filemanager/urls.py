from django.urls import path
from filemanager import views

urlpatterns = [
    #Filemanager URLs
    path('', views.filemanager,name='filemanager'),
    path('subirArchivo', views.subirArchivo,name='subirArchivo'),
    path('listarArchivos', views.listarArchivos,name='listarArchivos'),

]    