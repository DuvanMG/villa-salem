from django.apps import AppConfig
import os
from django.conf import settings

class PrestamosConfig(AppConfig):
    name = 'prestamos'
    path = os.path.join(settings.BASE_DIR, 'prestamos')

