from dataclasses import fields
from django import forms
from django.forms import ModelForm
from .models import Cobros, Prestamos, Fiador

class PrestamosForm(ModelForm):
    class Meta:
        model = Prestamos
        fields = ['FechaPrestamo','NombrePrestamo','ValorInicialPrestamo','ValorPorcentajePrestamo','TiempoPrestamo','TipoPrestamo']

class FiadorForm(ModelForm):
    class Meta:
        model = Fiador
        fields = ['Prestamo_Id','CelularPrestamo','DireccionPrestamoCasa','DireccionPrestamoTrabajo','NombreFiador','CelularFiador','DireccionFiadorCasa','DireccionFiadorTrabajo']

class CobrosForm(ModelForm):
    class Meta:
        model = Cobros
        fields = ['FechaCobro','NombreCobro','ValorCobro','TipoCobro']