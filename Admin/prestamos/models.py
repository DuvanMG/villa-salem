from django.db import models

# Create your models here.

class Prestamos(models.Model):
    Id_Prestamo = models.AutoField(primary_key=True, serialize=False)
    FechaPrestamo = models.CharField(max_length=70, default='', null=False)
    NombrePrestamo = models.CharField(max_length=70, default='', null=False)
    ValorInicialPrestamo = models.CharField(max_length=50, default='', null=False)
    ValorPorcentajePrestamo = models.CharField(max_length=50, default='', null=False)
    TiempoPrestamo = models.CharField(max_length=70, default='', null=False)
    TipoPrestamo = models.CharField(max_length=70, default='', null=False)

    class Meta:
        ordering = ('-FechaPrestamo','-Id_Prestamo')

class Fiador(models.Model):
    Id_Fiador = models.AutoField(primary_key=True, serialize=False)
    Prestamo_Id = models.OneToOneField(Prestamos, on_delete = models.CASCADE, null=False)
    CelularPrestamo = models.CharField(max_length=70, default='', null=False)
    DireccionPrestamoCasa = models.CharField(max_length=70, default='', null=False)
    DireccionPrestamoTrabajo = models.CharField(max_length=70, default='', null=False)
    NombreFiador = models.CharField(max_length=70, default='', null=False)
    CelularFiador = models.CharField(max_length=70, default='', null=False)
    DireccionFiadorCasa = models.CharField(max_length=70, default='', null=False)
    DireccionFiadorTrabajo = models.CharField(max_length=70, default='', null=False)

    def __str__(self):
        return self.NombreFiador

class Cobros(models.Model):
    Id_Cobro = models.AutoField(primary_key=True, serialize=False)
    FechaCobro = models.CharField(max_length=70, default='', null=False)
    NombreCobro = models.CharField(max_length=70, default='', null=False)
    ValorCobro = models.CharField(max_length=50, default='', null=False)
    TipoCobro = models.CharField(max_length=70, default='', null=False)

    class Meta:
        ordering = ('-FechaCobro','-Id_Cobro')