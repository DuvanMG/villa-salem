from django.urls import path
from prestamos import views

urlpatterns = [
    #Prestamos
    path('',views.PrestamosView.as_view(),name='tables-prestamos'),
    path('CrearFiador/',views.CrearFiador,name='CrearFiador'),
    path('updateFiador/<str:Id_Fiador>',views.updateFiador,name='updateFiador'),
    path('updatePrestamo/<str:Id_Prestamo>',views.updatePrestamo,name='updatePrestamo'),
    path('deletePrestamo/<str:Id_Prestamo>',views.deletePrestamo,name='deletePrestamo'),

    #Cobros
    path('CrearCobro/',views.CobrosView,name='CrearCobro'),
    path('updateCobro/<str:Id_Cobro>',views.updateCobro,name='updateCobro'),
    path('deleteCobro/<str:Id_Cobro>',views.deleteCobro,name='deleteCobro')
]    