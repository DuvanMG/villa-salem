from django.contrib import admin
from .models import Cobros, Prestamos, Fiador
#Register your models here.
admin.site.register(Prestamos)
admin.site.register(Fiador)
admin.site.register(Cobros)