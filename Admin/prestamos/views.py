from multiprocessing import context
from urllib import request
from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Prestamos, Fiador, Cobros
from .forms import PrestamosForm, FiadorForm, CobrosForm

# Create your views here.
class PrestamosView(LoginRequiredMixin,View):

    def get(self,request):

        heading = "Prestamos"
        pageview = "Prestamos & Cobros"

        #convirtiendo el objeto en un diccionario
        prestamos = Prestamos.objects.all()
        fiador = Fiador.objects.select_related('Prestamo_Id')
        cobros = Cobros.objects.all()
        context = {
            'Prestamos':prestamos,
            'Fiador':fiador,
            'Cobros':cobros,
            'heading':heading,
            'pageview':pageview,
            'form':PrestamosForm()
        }

        return render (request,'components/tables/components-prestamos.html',context) 
         
    def post(self,request):

        #convirtiendo el objeto en un diccionario
        formulario = PrestamosForm(request.POST)
        if formulario.is_valid:
            formulario.save()
            return redirect('/prestamos')
         
def CrearFiador(request):

    if request.method == 'POST':

        #convirtiendo el objeto en un diccionario
        formulario = FiadorForm(request.POST)
        if formulario.is_valid:
            formulario.save()
            return redirect('/prestamos')

def updateFiador(request,Id_Fiador):

    if request.method == 'POST':

        #convirtiendo el objeto en un diccionario
        form = Fiador.objects.get(Id_Fiador = Id_Fiador)
        formulario = FiadorForm(request.POST, instance = form)
        
        if formulario.is_valid:
            formulario.save()
            return redirect('/prestamos')

def updatePrestamo(request,Id_Prestamo):

    if request.method == 'POST':
    
        #convirtiendo el objeto en un diccionario
        form = Prestamos.objects.get(Id_Prestamo = Id_Prestamo)
        formulario = PrestamosForm(request.POST, instance = form)
        
        if formulario.is_valid:
            formulario.save()
            return redirect('/prestamos')

def deletePrestamo(request,Id_Prestamo):

    #convirtiendo el objeto en un diccionario
    Prestamos.objects.filter(Id_Prestamo = Id_Prestamo).delete()

    return redirect('/prestamos')

# Create your views here - Cobros.
class CobrosView(View,LoginRequiredMixin):
       
    def get(self,request):

        heading = "Prestamos"
        pageview = "Prestamos & Cobros"

        #convirtiendo el objeto en un diccionario
        data =  Cobros.objects.all()

        context = {
            'data':data,
            'heading':heading,
            'pageview':pageview,
            'form':CobrosForm()
        }
  
        return render(request,'components/tables/components-prestamos.html',context)
         
def CobrosView(request):

    if request.method == 'POST':
        #convirtiendo el objeto en un diccionario        
        formulario = CobrosForm(request.POST)
        if formulario.is_valid:
            formulario.save()
            return redirect('/prestamos')

def updateCobro(request,Id_Cobro):

    if request.method == 'POST':
        #convirtiendo el objeto en un diccionario
        form = Cobros.objects.get(Id_Cobro = Id_Cobro)

        formulario = CobrosForm(request.POST, instance = form)
        if formulario.is_valid:
            formulario.save()
            return redirect('/prestamos')


def deleteCobro(request,Id_Cobro):

    #convirtiendo el objeto en un diccionario
    Cobros.objects.filter(Id_Cobro = Id_Cobro).delete()

    return redirect('/prestamos')