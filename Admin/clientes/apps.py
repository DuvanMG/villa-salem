from django.apps import AppConfig
import os
from django.conf import settings

class ClientesConfig(AppConfig):
    name = 'clientes'
    path = os.path.join(settings.BASE_DIR, 'clientes')
