from django.db import models

# Create your models here.

class Clientes(models.Model):
    Id_Cliente = models.AutoField(primary_key=True, serialize=False)
    NombreIglesia = models.CharField(max_length=70, default='', null=False)
    NombreLider = models.CharField(max_length=70, default='', null=False)
    Contacto = models.CharField(max_length=70, default='', null=False)
    UbicacionCiudad = models.CharField(max_length=70, default='', null=False)

    class Meta:
        ordering = ('-Id_Cliente',)