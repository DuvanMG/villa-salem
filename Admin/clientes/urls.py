from django.urls import path
from clientes import views

urlpatterns = [
    #Cobros
    path('',views.ClientesView.as_view(),name='table-clientes'),
    path('updateCliente/<str:Id_Cliente>',views.updateCliente,name='updateCliente'),
    path('deleteCliente/<str:Id_Cliente>',views.deleteCliente,name='deleteCliente')
]