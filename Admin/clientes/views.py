from multiprocessing import context
from urllib import request
from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Clientes
from .forms import ClientesForm

# Create your views here - Contratos.
class ClientesView(LoginRequiredMixin,View):

    def get(self,request):

        heading = "Tabla de Clientes"
        pageview = "Finca"

        #convirtiendo el objeto en un diccionario
        Cliente = Clientes.objects.all()

        context = {
            'Clientes':Cliente,
            'heading':heading,
            'pageview':pageview,
            'form':ClientesForm()
        }

        return render (request,'components/tables/components-clientes.html',context) 
         
    def post(self,request):
        #convirtiendo el objeto en un diccionario
        formulario = ClientesForm(request.POST)
        if formulario.is_valid:
            formulario.save()
            return redirect('/clientes')
    
def updateCliente(request,Id_Cliente):

    if request.method == 'POST':
        #convirtiendo el objeto en un diccionario
        form = Clientes.objects.get(Id_Cliente = Id_Cliente)
        formulario = ClientesForm(request.POST, instance = form)
        
        if formulario.is_valid:
            formulario.save()
            return redirect('/clientes')

def deleteCliente(request,Id_Cliente):

    #convirtiendo el objeto en un diccionario
    Clientes.objects.filter(Id_Cliente = Id_Cliente).delete()

    return redirect('/clientes')