from django.apps import AppConfig
import os
from django.conf import settings

class LayoutConfig(AppConfig):
    name = 'layout'
    path = os.path.join(settings.BASE_DIR, 'layout')

