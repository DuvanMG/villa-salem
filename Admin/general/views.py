from multiprocessing import context
from urllib import request
from django.shortcuts import redirect, render
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core import serializers
from .models import General
from prestamos.models import Cobros, Prestamos
from arriendos.models import Arriendos
from creditos.models import Creditos, Cuotas
from .forms import GeneralForm
from django.db.models import Sum
# Create your views here.
class GastosView(View,LoginRequiredMixin):
       
    def get(self,request):

        heading = "General"
        pageview = "Villa Salem"  

        #convirtiendo el objeto en un diccionario
        data =  General.objects.all()
        Credito =  Creditos.objects.all().aggregate(ValorCredito=Sum('ValorCredito'))
        Cuota =  Cuotas.objects.all().aggregate(ValorCuota=Sum('ValorCuota'))
        Prestamo =  Prestamos.objects.all().aggregate(ValorInicialPrestamo=Sum('ValorInicialPrestamo'))
        Cobro =  Cobros.objects.all().aggregate(ValorCobro=Sum('ValorCobro'))
        Arriendo =  Arriendos.objects.all().aggregate(ValorArriendo=Sum('ValorArriendo'))
        Recibi = General.objects.filter(TipoGeneral = 'Recibi').aggregate(ValorGeneral=Sum('ValorGeneral'))
        Gaste = General.objects.filter(TipoGeneral = 'Gaste').aggregate(ValorGeneral=Sum('ValorGeneral'))

        credito = Credito.get("ValorCredito")
        cuotas = Cuota.get("ValorCuota")
        prestamos = Prestamo.get("ValorInicialPrestamo")
        cobros = Cobro.get("ValorCobro")
        arriendo = Arriendo.get("ValorArriendo")
        recibiGeneral = Recibi.get("ValorGeneral")
        gasteGeneral = Gaste.get("ValorGeneral")
        
        gaste = gasteGeneral + prestamos + cuotas
        recibi = recibiGeneral + arriendo + cobros + credito
        saldo = recibi - gaste

        context = {
            'recibi':recibi,
            'gaste':gaste,
            'saldo':saldo,
            'data':data,
            'heading':heading,
            'pageview':pageview,
            'form':GeneralForm()
        }
  
        return render(request,'components/tables/components-general.html',context)
         
    def post(self,request):

        formulario = GeneralForm(request.POST)
        if formulario.is_valid:
            formulario.save()
            return redirect('/general')

        heading = "General"
        pageview = "Villa Salem"  
        
        #convirtiendo el objeto en un diccionario
        data = General.objects.all()

        context = {
            'data':data,
            'heading':heading,
            'pageview':pageview,
            'form':GeneralForm()
        }

        return render(request,'components/tables/components-general.html',context)

def updateGeneral(request,Id_General):

    if request.method == 'POST':
        #convirtiendo el objeto en un diccionario
        form = General.objects.get(Id_General = Id_General)

        formulario = GeneralForm(request.POST, instance = form)
        if formulario.is_valid:
            formulario.save()
            return redirect('/general')


def deleteGeneral(request,Id_General):

    #convirtiendo el objeto en un diccionario
    General.objects.filter(Id_General = Id_General).delete()

    return redirect('/general')