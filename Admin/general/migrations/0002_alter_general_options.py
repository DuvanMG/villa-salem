# Generated by Django 4.1.1 on 2022-11-17 23:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='general',
            options={'ordering': ('-FechaGeneral', '-Id_General')},
        ),
    ]
