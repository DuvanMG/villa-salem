from django.db import models
from .choices import tipos
# Create your models here.

class General(models.Model):
    Id_General = models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")
    FechaGeneral = models.CharField(max_length=70, default='', null=False)
    ValorGeneral = models.BigIntegerField()
    TipoGeneral = models.CharField(max_length=70, choices=tipos, default='')
    DetalleGeneral = models.CharField(max_length=300, default='', null=False)

    class Meta:
        ordering = ('-FechaGeneral','-Id_General')