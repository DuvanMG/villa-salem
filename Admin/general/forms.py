from dataclasses import fields
from django import forms
from django.forms import ModelForm
from .models import General

class GeneralForm(ModelForm):
    class Meta:
        model = General
        fields = ['FechaGeneral','ValorGeneral','TipoGeneral','DetalleGeneral']