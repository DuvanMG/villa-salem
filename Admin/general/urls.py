from django.urls import path
from general import views
urlpatterns = [
    #general
    path('',views.GastosView.as_view(),name='tables-general'),
    path('updateGeneral/<str:Id_General>',views.updateGeneral,name='updateGeneral'),
    path('deleteGeneral/<str:Id_General>',views.deleteGeneral,name='deleteGeneral')
]     