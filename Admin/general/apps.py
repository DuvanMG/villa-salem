from django.apps import AppConfig
import os
from django.conf import settings

class GeneralConfig(AppConfig):
    name = 'general'
    path = os.path.join(settings.BASE_DIR, 'general')

